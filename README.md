Atividade

Se você ainda não construiu o contador, faça isso agora.
Impedir que o contador desça além de 0.
Impedir que o contador suba além de 20.

Créditos Bônus

Fazer com que o contador fique vermelho enquanto estiver em 10 ou mais.
Mudá-lo para a cor original se ele descer abaixo de 10.
Documentação: Styling and CSS

Super Mega Crédito Bônus

Permitir que a contagem inicial seja definida a partir de um prop.
Se uma contagem inicial não for passada, o contador deve começar em 0.

Super Hiper Mega Crédito Bônus

Adicionar um botão para reiniciar o contador.
Exiba o botão apenas se o contador tiver sido modificado em relação ao seu estado inicial.
Documentação: Conditional rendering