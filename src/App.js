import React from "react";

import "./App.css";

import Counter from "./Components/Counter";

function App() {
  return <Counter initialValue={0} />;
}

export default App;
