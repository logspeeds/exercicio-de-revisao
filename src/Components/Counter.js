import React from "react";

import "./Counter.css";

class Counter extends React.Component {
  state = { initialValue: this.props.initialValue, clicked: false };

  countUp = () => {
    if (this.state.initialValue < 20) {
      return this.setState({ initialValue: this.state.initialValue + 1 });
    }
  };

  countDown = () => {
    if (this.state.initialValue > 0) {
      return this.setState({ initialValue: this.state.initialValue - 1 });
    }
  };

  CounterUpFunction = () => {
    if (
      this.state.initialValue > 20 ||
      this.state.initialValue < 0 ||
      this.state.initialValue === ""
    ) {
      return this.setState({ initialValue: 0 });
    } else {
      this.setState({ clicked: true });
      return this.countUp();
    }
  };

  CounterDownFunction = () => {
    if (
      this.state.initialValue > 20 ||
      this.state.initialValue < 0 ||
      this.state.initialValue === ""
    ) {
      return this.setState({ initialValue: 0 });
    } else {
      this.setState({ clicked: true });
      return this.countDown();
    }
  };

  toZero = () => {
    this.setState({ initialValue: 0 });
    this.setState({ clicked: false });
  };

  render() {
    let className = "";
    if (this.state.initialValue >= 10) {
      className = "red";
    } else {
      className = "black";
    }
    if (this.state.clicked === false) {
      return (
        <>
          <div id="counterBox">
            <div>
              <h1>
                CONTADOR:
                <div className={className}>{this.state.initialValue}</div>
              </h1>
            </div>
          </div>
          <div className="buttons">
            <div className="buttons2">
              <button onClick={() => this.CounterDownFunction()}>
                {"<<"} REGRESSIVA
              </button>
              <button onClick={() => this.CounterUpFunction()}>
                PROGRESSIVA {">>"}
              </button>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div id="counterBox">
            <div>
              <h1>
                CONTADOR:
                <div className={className}>{this.state.initialValue}</div>
              </h1>
            </div>
            <div className="buttons">
              <div className="buttons2">
                <button id="redButton" onClick={() => this.toZero()}>ZERAR</button>
              </div>
            </div>
          </div>
          <div className="buttons">
            <div className="buttons2">
              <button onClick={() => this.CounterDownFunction()}>
                {"<<"} REGRESSIVA
              </button>{" "}
              <button onClick={() => this.CounterUpFunction()}>
                PROGRESSIVA {">>"}
              </button>
            </div>
          </div>
        </>
      );
    }
  }
}

export default Counter;
